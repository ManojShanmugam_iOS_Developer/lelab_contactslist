//
//  ContactTVCell.swift
//  LeLav-ContactsList
//
//  Created by Manoj Shanmugam on 11/04/20.
//  Copyright © 2020 Shenll Technology Solutions. All rights reserved.
//

import UIKit

class ContactTVCell: UITableViewCell {

    // MARK: UILabel Declaration
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var phoneNumberLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
