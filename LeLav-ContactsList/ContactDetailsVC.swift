//
//  ContactDetailsVC.swift
//  LeLav-ContactsList
//
//  Created by Manoj Shanmugam on 11/04/20.
//  Copyright © 2020 Shenll Technology Solutions. All rights reserved.
//

import UIKit

class ContactDetailsVC: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    // MARK: UITableView Declaration
    @IBOutlet weak var ContactDetailsTableView: UITableView?
    //MARK: - NSDictionary Declaration
    var userDictionary : NSDictionary?
    
    // MARK: UITextField Declaration
    var keysArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("User Dictionary",self.userDictionary)
        //ContactDetailsTableView
        self.ContactDetailsTableView!.allowsMultipleSelectionDuringEditing = false
        // Do any additional setup after loading the view.
        
        self.keysArray = [ID,NAME,USERNAME,PHONE,EMAIL,COMPANY,ADDRESS]
    }
    
    
    // MARK: UIButton Action functions
    @IBAction func actionSendMessageButton(_sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.keysArray.count
    }
    
    // MARK: UITableView Datasource functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTVCell") as! ContactTVCell
        let key = self.keysArray.object(at: indexPath.section) as! String
        var valueString = self.userDictionary![key] as? String
        if(key == ID){
            valueString = (self.userDictionary![key] as! NSNumber).stringValue
        }
        
        cell.nameLabel.text = valueString
        cell.phoneNumberLabel.text = key.uppercased()
        cell.nameLabel.sizeToFit()
        return cell
    }
}
