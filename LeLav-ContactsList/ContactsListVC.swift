//
//  ContactsListVC.swift
//  LeLav-ContactsList
//
//  Created by Manoj Shanmugam on 11/04/20.
//  Copyright © 2020 Shenll Technology Solutions. All rights reserved.
//

import UIKit

class ContactsListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    // MARK: UITableView Declaration
    @IBOutlet weak var ContactsTableView: UITableView?
    
    //MARK: - NSDictionary Declaration
    var userDictionary : NSDictionary?
    
    // MARK: NSObject Declaration
    let usersObject = UsersDBModel()
    let utilObject = UtilFile()
    
    // MARK: UITextField Declaration
    var contactsArray = NSArray()
    
    
    
    // MARK: UIViewController functions
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.ContactsTableView!.allowsMultipleSelectionDuringEditing = false
        
        if(utilObject.IsNetworkConnected()) {
            self.getContactsFromApi()
        } else {
            self.contactsArray = self.usersObject.getUsersFromDB() as NSArray
        }
    }
    
    // MARK: UITableView Datasource functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contactsArray.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTVCell") as! ContactTVCell
        
        let contactDictionary = self.contactsArray.object(at: indexPath.row) as! NSDictionary
        cell.nameLabel.text = contactDictionary[NAME] as? String
        cell.phoneNumberLabel.text = contactDictionary[PHONE] as? String
        cell.nameLabel.sizeToFit()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.userDictionary = (self.contactsArray.object(at: indexPath.row) as! NSDictionary)
        self.performSeague("ContactDetailsSegue")
    }
    
    //MARK: - PerformSeague
    func performSeague(_ segueIdentifier: String?) {
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: segueIdentifier ?? "", sender: self)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "ContactDetailsSegue"){
            let contactDetailsVC = segue.destination as! ContactDetailsVC
            contactDetailsVC.userDictionary = self.userDictionary
        }
        
    }
    
    func getContactsFromApi(){
        // Create URL
        let url = URL(string: "https://jsonplaceholder.typicode.com/users")
        guard let requestUrl = url else { fatalError() }
        // Create URL Request
        var request = URLRequest(url: requestUrl)
        // Specify HTTP Method to use
        request.httpMethod = "GET"
        // Send HTTP Request
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            // Check if Error took place
            if let error = error {
                print("Error took place \(error)")
                return
            }
            
            // Read HTTP Response Status code
            if let response = response as? HTTPURLResponse {
                print("Response HTTP Status code: \(response.statusCode)")
            }
            
            // Convert HTTP Response Data to a JSON
            if let data = data {
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                        print("Response Array \(jsonResult)")
                        self.usersObject.insertOrUpdateDataFromResponse(contactsArray: jsonResult)
                        self.contactsArray = self.usersObject.getUsersFromDB() as NSArray
                        DispatchQueue.main.asyncAfter(deadline: .now()) {
                            self.ContactsTableView?.reloadData()
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
        task.resume()
    }
}
