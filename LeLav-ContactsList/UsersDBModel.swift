//
//  UsersDBModel.swift
//  LeLav-ContactsList
//
//  Created by Manoj Shanmugam on 11/04/20.
//  Copyright © 2020 Shenll Technology Solutions. All rights reserved.
//

import UIKit

let ID = "id"
let NAME = "name"
let USERNAME = "username"
let EMAIL = "email"
let ADDRESS = "address"
let PHONE = "phone"
let WEBSITE  = "website"
let COMPANY = "company"
let TBL_USERS = "tbl_users"

let DB_NAME  = "user"
let FULL_DB_NAME  = "user.sqlite"
let DB_FOLDER  = "/DatabaseFolder"

class UsersDBModel: NSObject {
    
    let utilObject = UtilFile()
    
    func userArray() -> [Any] {
        var userKeyArray = [Any]()
        var onceToken: Int = 0
        
        if (onceToken == 0) {
            /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
            userKeyArray = [ID,NAME,USERNAME,EMAIL,PHONE,WEBSITE,COMPANY,ADDRESS]
        }
        onceToken = 1
        return userKeyArray
    }
    
    func getContactDic(forDBTask dicFromApiResponse: [AnyHashable: Any]) -> [AnyHashable: Any] {
        var childDicForDBTask = [AnyHashable: Any]()
        let arraTaks = self.userArray()
        for dbColumnIndex in 0..<self.userArray().count {
            let dicV = arraTaks[dbColumnIndex] as! String
            let identifier = dicFromApiResponse[dicV] as Any
            childDicForDBTask[dicV] = identifier
        }
        return childDicForDBTask
    }
    
    func insertOrUpdateDataFromResponse(contactsArray: NSArray){
        deleteUsers()
        let modelManagerInstance = ModelManager.getInstance()
        for userDict in contactsArray{
            var userDictionary = userDict as? [AnyHashable: Any]
            let id  = userDictionary?[ID] as! NSNumber
            let addressDict = userDictionary?[ADDRESS] as! NSDictionary
            let address = (addressDict["suite"] as! String) + ",\n" + (addressDict["street"] as! String) + ",\n" + (addressDict["city"] as! String) + " - " + (addressDict["zipcode"] as! String) + "."
            userDictionary![ADDRESS] = address
            
            let companyDict = userDictionary?[COMPANY] as! NSDictionary
            let company = (companyDict["name"] as! String) + ",\n" + (companyDict["catchPhrase"] as! String) + ",\n" + (companyDict["bs"] as! String) + "."
            userDictionary![COMPANY] = company
            let selectQuerySql = "select count(\(ID)) from \(TBL_USERS) where \(ID)=\("'\(id)'")"
            let userCount: Int = Int(modelManagerInstance!.getTableRecordCount(selectQuerySql))
            if userCount > 0 {
                modelManagerInstance?.updateData(inDb: TBL_USERS, keyValueDic: self.getContactDic(forDBTask: userDictionary!), whereFieldName: ID, whereFieldValue: id.stringValue)
            }
            else {
                modelManagerInstance?.insertData(inDb: TBL_USERS, keyValueDic: self.getContactDic(forDBTask: userDictionary!))
            }
        }
    }
    
    func insertUsersDetail(_ msgDictionary: [String: Any]) {
        let modelManagerInstance = ModelManager.getInstance()
        modelManagerInstance?.insertData(inDb: TBL_USERS, keyValueDic: self.getContactDic(forDBTask: msgDictionary))
    }

    func updateUsersDetail(_ msgDictionary: [String: Any]) {

        let modelManagerInstance = ModelManager.getInstance()
        let messageId = msgDictionary[ID] as! String

        modelManagerInstance?.updateData(inDb: TBL_USERS, keyValueDic: self.getContactDic(forDBTask: msgDictionary), whereFieldName: ID, whereFieldValue: messageId)
    }
    
    func getUsersFromDB() -> [Any] {
        let modelManagerInstance = ModelManager.getInstance()
        var arrChild = [Any]()
        var strQry = ""
        strQry = "select * from \(TBL_USERS) ORDER BY \(NAME) ASC"
        let arrSelectedTeams = modelManagerInstance?.selectData(strQry)
        arrSelectedTeams?.enumerateObjects({ item, index, _ in
        })
        arrChild = arrSelectedTeams as! [Any]
        return arrChild
    }

    func getUsersCountDetail() -> Int {
        let modelManagerInstance = ModelManager.getInstance()
        let selectQuerySql = "select count(*) from \(TBL_USERS)"
        let kitsCount: Int = Int(modelManagerInstance!.getTableRecordCount(selectQuerySql))
        return kitsCount
    }

    func deleteUserWithWhereQuery(columnName : String, equalsMyValue : String){
        let modelManagerInstance = ModelManager.getInstance()
        let deleteQuerySql = "DELETE FROM \(TBL_USERS) where \(columnName)=\("'\(equalsMyValue)'")"
        modelManagerInstance?.deleteData(deleteQuerySql)
    }
    
    func deleteUsers(){
        let modelManagerInstance = ModelManager.getInstance()
        let deleteQuerySql = "DELETE FROM \(TBL_USERS)"
        modelManagerInstance?.deleteData(deleteQuerySql)
    }


}
