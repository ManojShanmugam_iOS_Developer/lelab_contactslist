
import Foundation
import UIKit
import SystemConfiguration
import AVFoundation

class UtilFile
{
    
    var masterDatabase: FMDatabase?
    var resultSet: FMResultSet?

    func QuitApp(){
        exit(0);
    }

    func create_folder(foldername : String) {
        var success : Bool = Bool()
        success = false
        let fileManager = FileManager.default
        //let documentsPath1 = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let documentsPath1 = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true)[0])
        let logsPath = documentsPath1.appendingPathComponent(foldername)?.path
        success = fileManager.fileExists(atPath: logsPath!)
        print(logsPath!)
        
        if(!success){
            do {
                try FileManager.default.createDirectory(atPath: logsPath!, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                print("Unable to create directory \(error.debugDescription)")
            }
        }else{
            print("folder exist")
        }
        
    }
    
    func IsNetworkConnected() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags){
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    

    func isDatabaseFileExistsOrNot(file : NSString) -> Bool {
        
        var isDBPathAvailable : Bool = Bool()
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent(file as String).path
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: filePath) {
            isDBPathAvailable = true
        } else {
            isDBPathAvailable = false
        }
        return isDBPathAvailable
    }
    
    func copyFile(file : NSString)  {
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        let urlPath: URL? = self.applicationHiddenDocumentsDirectory(str_folder: DB_FOLDER as NSString) as URL
        //  let paths:String = urlPath!.path
        let strWritablePath: String = URL(fileURLWithPath: (urlPath?.path)!).appendingPathComponent(FULL_DB_NAME).path
        let filePath = url.appendingPathComponent(file as String).path
        let fileManager = FileManager.default
        
        if (!fileManager.fileExists(atPath: filePath)) {
            let bundlePath = Bundle.main.path(forResource: file as String, ofType: ".sqlite")
            print("bundlePath",bundlePath as Any)
            do
            {
                try fileManager.copyItem(atPath: bundlePath!, toPath: strWritablePath)
            }
            catch
            {
                print(error)
            }
        }
    }
    
    func applicationHiddenDocumentsDirectory(str_folder : NSString) -> NSURL{
        
        let libraryPath:String = NSSearchPathForDirectoriesInDomains(.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let path = libraryPath.appending(str_folder as String)
        let pathURl:NSURL =  NSURL.fileURL(withPath: path) as NSURL
        var isDirectory:ObjCBool = false
        if(FileManager.default.fileExists(atPath: path, isDirectory: &isDirectory))
        {
            print("applicationHiddenDocumentsDirectory",isDirectory)
            
            if(isDirectory).boolValue
            {
                return pathURl
            }
            else
            {
                NSException.raise(NSExceptionName(rawValue: "Private Documents exists, and is a file"), format: "Path: %@", arguments:getVaList([path]))
                
            }
            
        }
        do {
            
            try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        return pathURl
    }
    
    func openDatabase() {
        let util = UtilFile()
        let url: URL? = self.applicationHiddenDocumentsDirectory(str_folder: DB_FOLDER as NSString) as URL
        let strWritablePath: String = URL(fileURLWithPath: (url?.relativePath)!).appendingPathComponent(FULL_DB_NAME).absoluteString
        util.masterDatabase = FMDatabase(path: strWritablePath)
        if (util.masterDatabase?.open())! {
            print("Open Database Success")
            print("Open Database path",strWritablePath)
        }
    }

    func createDirectory(_ folderName : String){
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(folderName)
        if !fileManager.fileExists(atPath: paths){
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
        }else{
            print("Already dictionary created.")
        }
    }
    
    func getDirectoryPath(_ folderName : String) -> String {
        // let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(folderName)
        return paths
    }

}
