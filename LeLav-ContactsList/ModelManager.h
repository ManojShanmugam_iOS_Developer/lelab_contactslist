//
//  ModelManager.h
//


#import <Foundation/Foundation.h>
#import <FMDB/FMDB.h>


@interface ModelManager : NSObject

@property (nonatomic,strong) FMDatabase *database;

+(ModelManager *) getInstance;

/*
 This is model methods. You need to create your own methods for your objects with your class name instea of "Model".
 */

-(void)insertData:(NSString *)strInsertQuery;
-(void)updateData:(NSString *)strUpdateQuery;
-(void)deleteData:(NSString *)strDeleteQuery;
-(NSString *)selectColumn:(NSString *)strSelectQuery andColumnName:(NSString *)strColumnName;
-(NSMutableArray *)selectData:(NSString *)strSelectQuery;
-(int)getTableRecordCount:(NSString *)countQuery;
-(void)insertDataInDb:(NSString *)insertTableName keyValueDic:(NSDictionary *)insertKeyValueDic;
-(void)updateDataInDb:(NSString *)updateTableName keyValueDic:(NSDictionary *)updateKeyValueDic whereFieldName:(NSString *)whereField whereFieldValue:(NSString *)whereValue;
-(void)updateAllData:(NSString *)updateTableName keyAllValueDic:(NSDictionary *)updateKeyValueDic;
/*
 This is another method to insert records. User if you require this.
 */
-(void)insertDataInDbMethod1:(NSString *)insertTableName keyValueDic:(NSDictionary *)insertKeyValueDic;

-(void)updateMoreConditionDataInDb:(NSString *)updateTableName keyValueDic:(NSDictionary *)updateKeyValueDic whereFieldName:(NSDictionary *)updateWherKeyValueDic ;

- (BOOL)executeBatchQuery:(NSString *)sql;
@end
