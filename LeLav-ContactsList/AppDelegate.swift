//
//  AppDelegate.swift
//  LeLav-ContactsList
//
//  Created by Manoj Shanmugam on 11/04/20.
//  Copyright © 2020 Shenll Technology Solutions. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let utilObject = UtilFile()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //DB creating
        utilObject.copyFile(file: "user")
        utilObject.openDatabase()
        return true
    }
    
    
}

