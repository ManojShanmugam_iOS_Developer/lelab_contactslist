//
//  ModelManager.m
//

#import "ModelManager.h"
//#import "LeLav-ContactsList-Swift.h"
#import "LeLav-ContactsList-Bridging-Header.h"

@class Constatns;
@implementation ModelManager

static ModelManager *instance=nil;
@synthesize database=_database;

+(ModelManager *) getInstance
{
    if(!instance){
        instance=[[ModelManager alloc]init];
        NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
        NSString *path = [libraryPath stringByAppendingPathComponent:@"/DatabaseFolder"];
        NSURL *pathURL = [NSURL fileURLWithPath:path];
        NSString *dbPath = [[pathURL relativePath] stringByAppendingPathComponent:@"user.sqlite"];
        instance.database=[FMDatabase databaseWithPath:dbPath];
    }
    return instance;
}

-(void)insertData:(NSString *)strInsertQuery
{
    NSLog(@"Insert Query %@",strInsertQuery);
    [instance.database open];
    //!WARNING, Please use/uncomment the below line only if you need to encrypt your database.
    BOOL isInserted=[instance.database executeUpdate:strInsertQuery];
    [instance.database close];
    
    if(isInserted){
         NSLog(@"Insert Success");
    }
    else
    NSLog(@"Insert Error");
}

-(void)insertDataWithParameter:(NSString *)strInsertQueryWithParameter arrayParams:(NSDictionary *)insertParams
{
    NSLog(@"InsertQueryParamQuery %@",strInsertQueryWithParameter);
    [instance.database open];
    //!WARNING, Please use/uncomment the below line only if you need to encrypt your database.
    BOOL isInserted=[instance.database executeUpdate:strInsertQueryWithParameter withParameterDictionary:insertParams];
    [instance.database close];
    
    if(isInserted){
        NSLog(@"Insert Param method Success");
    }
    else
     NSLog(@"Insert Param method Error");
}

-(void)updateData:(NSString *)strUpdateQuery
{
    NSLog(@"Update Query %@",strUpdateQuery);
    [instance.database open];
    BOOL isUpdated=[instance.database executeUpdate:strUpdateQuery];
    [instance.database close];
    
    if(isUpdated){
        NSLog(@"Update Success");
    }
    else
     NSLog(@"Update Error");
}

-(void)deleteData:(NSString *)strDeleteQuery
{
    NSLog(@"Delete Query %@",strDeleteQuery);
    [instance.database open];
    BOOL isDeleted=[instance.database executeUpdate:strDeleteQuery];
    [instance.database close];
    
    if(isDeleted){
        NSLog(@"Delete Success");
    }
    else
    NSLog(@"Delete Error");
}

-(NSString *)selectColumn:(NSString *)strSelectQuery andColumnName:(NSString *)strColumnName
{
    //  [AppConstants_OBJC printLogKey:@"Select Column Query" printValue:strSelectQuery];
    [instance.database open];
    NSString * resultValue=@"";
    FMResultSet *resultSet=[instance.database executeQuery:strSelectQuery];
    
    if([resultSet next]){
        resultValue = [resultSet stringForColumn:strColumnName];
    }
    
    [instance.database close];
    return resultValue;
}

-(NSMutableArray *)selectData:(NSString *)strSelectQuery
{
    // [AppConstants_OBJC printLogKey:@"Select Query" printValue:strSelectQuery];
    NSMutableArray *results = [NSMutableArray array];
    [instance.database open];
    FMResultSet *resultSet=[instance.database executeQuery:strSelectQuery];
    
    if(resultSet){
        while([resultSet next])
            [results addObject:[resultSet resultDictionary]];
    }
    
    [instance.database close];
    return results;
    
}

-(int)getTableRecordCount:(NSString *)countQuery{
    // [AppConstants_OBJC printLogKey:@"Count Query" printValue:countQuery];
    [instance.database open];
    FMResultSet *resultSetRecord = [instance.database executeQuery:countQuery];
    int totalCount=0;
    
    if ([resultSetRecord next]) {
        totalCount = [resultSetRecord intForColumnIndex:0];
    }
    
    [instance.database close];
    //   [AppConstants_OBJC printLogKey:@"Table Record count" printValue:[NSString stringWithFormat:@"%d",totalCount]];
    return totalCount;
}

# pragma InsertQuery Formatting Function

-(void)insertDataInDb:(NSString *)insertTableName keyValueDic:(NSDictionary *)insertKeyValueDic{
    
    NSMutableArray* cols = [[NSMutableArray alloc] init];
    NSMutableArray* vals = [[NSMutableArray alloc] init];
    
    for (id key in insertKeyValueDic) {
        [cols addObject:key];
        [vals addObject:[insertKeyValueDic objectForKey:key]];
    }
    
    NSMutableArray* newCols = [[NSMutableArray alloc] init];
    NSMutableArray* newVals = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<[cols count]; i++) {
        /*[newCols addObject:[NSString stringWithFormat:@"'%@'", [cols objectAtIndex:i]]];
         [newVals addObject:[NSString stringWithFormat:@"'%@'", [vals objectAtIndex:i]]];*/
        [newCols addObject:[NSString stringWithFormat:@"'%@'", [cols objectAtIndex:i]]];
        
        if (![[vals objectAtIndex:i] isKindOfClass:[NSNull class]]){
            NSString * valueToString=[NSString stringWithFormat:@"%@", [vals objectAtIndex:i]];
            NSString * strReplaceSingleQuote=[valueToString stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            //            NSString * strReplaceSingleQuote=[[vals objectAtIndex:i] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            NSString * strWithoutSingleQuote=[NSString stringWithFormat:@"'%@'", strReplaceSingleQuote];
            [newVals addObject:strWithoutSingleQuote];
        }
        else{
            NSString * strWithoutSingleQuote=[NSString stringWithFormat:@"'%@'", @""];
            [newVals addObject:strWithoutSingleQuote];
        }
    }
    NSString* insertQuerySql = [NSString stringWithFormat:@"insert into %@ (%@) values (%@)", insertTableName,[newCols componentsJoinedByString:@", "], [newVals componentsJoinedByString:@", "]];
    
    [self insertData:insertQuerySql];
    
}

/*
 This is another method to insert records. User if you require this.
 */
-(void)insertDataInDbMethod1:(NSString *)insertTableName keyValueDic:(NSDictionary *)insertKeyValueDic{
    
    NSArray* keys = [insertKeyValueDic allKeys];
    NSMutableArray *prefixedKeys = [NSMutableArray array];
    [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [prefixedKeys addObject:[NSString stringWithFormat:@":%@",obj]];
    }];
    //NSLog(@"",prefixedKeys);
    NSString *addAircraftQuery = [NSString stringWithFormat: @"INSERT INTO %@ (%@) VALUES (%@)",insertTableName,[keys componentsJoinedByString:@","],[prefixedKeys componentsJoinedByString:@","]];
    [self insertDataWithParameter:addAircraftQuery arrayParams:insertKeyValueDic];
    
}

-(void)updateDataInDb:(NSString *)updateTableName keyValueDic:(NSDictionary *)updateKeyValueDic whereFieldName:(NSString *)whereField whereFieldValue:(NSString *)whereValue{
    
    NSMutableArray* cols = [[NSMutableArray alloc] init];
    NSMutableArray* vals = [[NSMutableArray alloc] init];
    
    for (id key in updateKeyValueDic) {
        [cols addObject:key];
        [vals addObject:[updateKeyValueDic objectForKey:key]];
    }
    
    NSMutableArray* newCols = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<[cols count]; i++) {
        /*[newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],[vals objectAtIndex:i]]];*/
        if (![[vals objectAtIndex:i] isKindOfClass:[NSNull class]]){
            NSString * valueToString=[NSString stringWithFormat:@"%@", [vals objectAtIndex:i]];
            NSString * strReplaceSingleQuote=[valueToString stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            //            NSString * strReplaceSingleQuote=[[vals objectAtIndex:i] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            
            [newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],strReplaceSingleQuote]];
        }
        else{
            [newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],@""]];
        }
    }
    
    NSString * fullUpdateString = [NSString stringWithFormat:@"%@",[newCols componentsJoinedByString:@", "]];
    NSString* updateQuerySql = [NSString stringWithFormat:@"UPDATE %@ SET %@ where %@='%@'", updateTableName,fullUpdateString,whereField,whereValue];
    
    [self updateData:updateQuerySql];
    
}
-(void)updateMoreConditionDataInDb:(NSString *)updateTableName keyValueDic:(NSDictionary *)updateKeyValueDic whereFieldName:(NSDictionary *)updateWherKeyValueDic {
    
    NSMutableArray* cols = [[NSMutableArray alloc] init];
    NSMutableArray* vals = [[NSMutableArray alloc] init];
    
    for (id key in updateKeyValueDic) {
        [cols addObject:key];
        [vals addObject:[updateKeyValueDic objectForKey:key]];
    }
    
    NSMutableArray* newCols = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<[cols count]; i++) {
        /*[newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],[vals objectAtIndex:i]]];*/
        if (![[vals objectAtIndex:i] isKindOfClass:[NSNull class]]){
            NSString * valueToString=[NSString stringWithFormat:@"%@", [vals objectAtIndex:i]];
            NSString * strReplaceSingleQuote=[valueToString stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            //            NSString * strReplaceSingleQuote=[[vals objectAtIndex:i] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            
            [newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],strReplaceSingleQuote]];
        }
        else{
            [newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],@""]];
        }
    }
    
    NSString * fullUpdateString = [NSString stringWithFormat:@"%@",[newCols componentsJoinedByString:@", "]];
    
    
    [cols removeAllObjects];
    [vals removeAllObjects];
    [newCols removeAllObjects];
    
    for (id key in updateWherKeyValueDic) {
        [cols addObject:key];
        [vals addObject:[updateWherKeyValueDic objectForKey:key]];
    }
    
    for (int i = 0; i<[cols count]; i++) {
        /*[newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],[vals objectAtIndex:i]]];*/
        if (![[vals objectAtIndex:i] isKindOfClass:[NSNull class]]){
            NSString * valueToString=[NSString stringWithFormat:@"%@", [vals objectAtIndex:i]];
            NSString * strReplaceSingleQuote=[valueToString stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            //            NSString * strReplaceSingleQuote=[[vals objectAtIndex:i] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            
            [newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],strReplaceSingleQuote]];
        }
        else{
            [newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],@""]];
        }
    }
    
    
    NSString *whereField = [NSString stringWithFormat:@"%@",[newCols componentsJoinedByString:@" and "]];
    
    
    NSString* updateQuerySql = [NSString stringWithFormat:@"UPDATE %@ SET %@ where %@", updateTableName,fullUpdateString,whereField];
    
    [self updateData:updateQuerySql];
    
}

-(void)updateAllData:(NSString *)updateTableName keyAllValueDic:(NSDictionary *)updateKeyValueDic {
    
    NSMutableArray* cols = [[NSMutableArray alloc] init];
    NSMutableArray* vals = [[NSMutableArray alloc] init];
    
    for (id key in updateKeyValueDic) {
        [cols addObject:key];
        [vals addObject:[updateKeyValueDic objectForKey:key]];
    }
    
    NSMutableArray* newCols = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<[cols count]; i++) {
        /*[newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],[vals objectAtIndex:i]]];*/
        if (![[vals objectAtIndex:i] isKindOfClass:[NSNull class]]){
            NSString * valueToString=[NSString stringWithFormat:@"%@", [vals objectAtIndex:i]];
            NSString * strReplaceSingleQuote=[valueToString stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            
            //            NSString * strReplaceSingleQuote=[[vals objectAtIndex:i] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            
            [newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],strReplaceSingleQuote]];
        }
        else{
            [newCols addObject:[NSString stringWithFormat:@"'%@' = '%@'", [cols objectAtIndex:i],@""]];
        }
    }
    
    NSString * fullUpdateString = [NSString stringWithFormat:@"%@",[newCols componentsJoinedByString:@", "]];
    NSString* updateQuerySql = [NSString stringWithFormat:@"UPDATE %@ SET %@", updateTableName,fullUpdateString];
    
    [self updateData:updateQuerySql];
    
}


- (BOOL)executeBatchQuery:(NSString *)sql
{
    BOOL success = false;
    [instance.database open];
    success = [instance.database executeStatements:sql];
    [instance.database close];
    
    if(success)
    NSLog(@"Batch Query Execution Success");
    else
    NSLog(@"Batch Query Execution Error");
    
    return true;
}

@end
